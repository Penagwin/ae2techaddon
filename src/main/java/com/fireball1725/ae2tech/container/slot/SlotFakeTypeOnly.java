package com.fireball1725.ae2tech.container.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

public class SlotFakeTypeOnly extends SlotFake {
    public SlotFakeTypeOnly(IInventory inv, int idx, int x, int y) {
        super (inv, idx, x, y);
    }

    public void putStack(ItemStack itemStack) {
        if (itemStack != null) {
            itemStack = itemStack.copy();
            if (itemStack.stackSize > 1) {
                itemStack.stackSize = 1;
            } else if (itemStack.stackSize < -1 ) {
                itemStack.stackSize = -1;
            }
        }

        super.putStack(itemStack);
    }
}
