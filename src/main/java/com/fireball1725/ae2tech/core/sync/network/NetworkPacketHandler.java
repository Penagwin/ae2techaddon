package com.fireball1725.ae2tech.core.sync.network;

import com.fireball1725.ae2tech.core.sync.NetworkPacket;
import com.fireball1725.ae2tech.core.sync.NetworkPacketHandlerBase;
import com.fireball1725.ae2tech.util.LogHelper;
import cpw.mods.fml.common.network.internal.FMLProxyPacket;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;

import java.lang.reflect.InvocationTargetException;

public final class NetworkPacketHandler extends NetworkPacketHandlerBase implements IPacketHandler {
    public void onPacketData(INetworkInfo manager, FMLProxyPacket packet, EntityPlayer player) {
        ByteBuf stream = packet.payload();
        int packetType = -1;
        try {
            packetType = stream.readInt();
            NetworkPacket pack = NetworkPacketHandlerBase.PacketTypes.getPacket(packetType).parsePacket(stream);
            pack.serverPacketData(manager, pack, player);
        } catch (InstantiationException e) {
            LogHelper.error(e);
        } catch (IllegalAccessException e) {
            LogHelper.error(e);
        } catch (IllegalArgumentException e) {
            LogHelper.error(e);
        } catch (InvocationTargetException e) {
            LogHelper.error(e);
        }
    }
}
