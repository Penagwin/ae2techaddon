package com.fireball1725.ae2tech.core.sync.network;

import cpw.mods.fml.common.network.internal.FMLProxyPacket;
import net.minecraft.entity.player.EntityPlayer;

public abstract interface IPacketHandler {
    public abstract void onPacketData(INetworkInfo paramINetworkInfo, FMLProxyPacket paramFMLProxyPacket, EntityPlayer paramEntityPlayer);
}
