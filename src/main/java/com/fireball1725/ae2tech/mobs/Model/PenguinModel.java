package com.fireball1725.ae2tech.mobs.Model;


import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

/**
 * Created by Penagwin on 8/24/2014.
 */
public class PenguinModel extends ModelBase
{
  //fields
    ModelRenderer WingL;
    ModelRenderer Body;
    ModelRenderer FootR;
    ModelRenderer FootL;
    ModelRenderer WingR;
    ModelRenderer Head;
    ModelRenderer Beak;
  
  public PenguinModel()
  {
    textureWidth = 64;
    textureHeight = 32;
    
      WingL = new ModelRenderer(this, 32, 4);
      WingL.addBox(0F, -1F, -2F, 2, 9, 3);
      WingL.setRotationPoint(-4F, 12F, -1F);
      WingL.setTextureSize(64, 32);
      WingL.mirror = true;
      setRotation(WingL, 0F, 3.141593F, 0F);
      Body = new ModelRenderer(this, 0, 0);
      Body.addBox(-4F, 0F, -4F, 8, 12, 8);
      Body.setRotationPoint(0F, 10F, 0F);
      Body.setTextureSize(64, 32);
      Body.mirror = true;
      setRotation(Body, 0F, 0F, 0F);
      FootR = new ModelRenderer(this, 32, 0);
      FootR.addBox(-1F, 0F, -1F, 2, 2, 2);
      FootR.setRotationPoint(2F, 22F, -2F);
      FootR.setTextureSize(64, 32);
      FootR.mirror = true;
      setRotation(FootR, 0F, 0F, 0F);
      FootL = new ModelRenderer(this, 32, 0);
      FootL.addBox(0F, 0F, -1F, 2, 2, 2);
      FootL.setRotationPoint(-3F, 22F, -2F);
      FootL.setTextureSize(64, 32);
      FootL.mirror = true;
      setRotation(FootL, 0F, 0F, 0F);
      WingR = new ModelRenderer(this, 32, 4);
      WingR.addBox(0F, -1F, -1F, 2, 9, 3);
      WingR.setRotationPoint(4F, 12F, -1F);
      WingR.setTextureSize(64, 32);
      WingR.mirror = true;
      setRotation(WingR, 0F, 0F, 0F);
      Head = new ModelRenderer(this, 0, 20);
      Head.addBox(-3F, -6F, -3F, 6, 6, 6);
      Head.setRotationPoint(0F, 10F, 0F);
      Head.setTextureSize(64, 32);
      Head.mirror = true;
      setRotation(Head, 0F, 0F, 0F);
      Beak = new ModelRenderer(this, 40, 0);
      Beak.addBox(-1F, -1F, -1F, 2, 3, 1);
      Beak.setRotationPoint(0F, 8F, -3F);
      Beak.setTextureSize(64, 32);
      Beak.mirror = true;
      setRotation(Beak, 0F, 0F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    WingL.render(f5);
    Body.render(f5);
    FootR.render(f5);
    FootL.render(f5);
    WingR.render(f5);
    Head.render(f5);
    Beak.render(f5);
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
  }

}
