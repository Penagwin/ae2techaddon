package com.fireball1725.ae2tech.mobs.Renderer;

import com.fireball1725.ae2tech.lib.Reference;
import com.fireball1725.ae2tech.mobs.Entity.PenguinEntity;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

/**
 * Created by Penagwin on 8/24/2014.
 */
@SideOnly(Side.CLIENT)
public class PenguinRenderer extends RenderLiving
{
    private static final ResourceLocation cowTextures = new ResourceLocation(Reference.MOD_ID, "textures/mobs/penguin.png");

    public PenguinRenderer(ModelBase p_i1253_1_, float p_i1253_2_)
    {
        super(p_i1253_1_, p_i1253_2_);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    protected ResourceLocation getEntityTexture(PenguinEntity p_110775_1_)
    {
        return cowTextures;
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    protected ResourceLocation getEntityTexture(Entity p_110775_1_)
    {
        return this.getEntityTexture((PenguinEntity)p_110775_1_);
    }
}