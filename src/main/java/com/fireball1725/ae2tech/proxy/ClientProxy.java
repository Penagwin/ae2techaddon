package com.fireball1725.ae2tech.proxy;


import com.fireball1725.ae2tech.mobs.Mobs;


public  class ClientProxy extends CommonProxy {
    @Override
    public void registerRenderer() {

        Mobs.registerAllWithRender();

    }
}
