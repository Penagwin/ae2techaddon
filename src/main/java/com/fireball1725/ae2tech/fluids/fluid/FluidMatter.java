package com.fireball1725.ae2tech.fluids.fluid;

import com.fireball1725.ae2tech.lib.Reference;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;
import scala.Int;

import java.util.Random;

/**
 * Created by Penagwin on 8/19/2014.
 */
public class FluidMatter extends BlockFluidClassic {

    @SideOnly(Side.CLIENT)
    protected IIcon stillIcon;
    @SideOnly(Side.CLIENT)
    protected IIcon flowingIcon;


    public FluidMatter() {
        super(new Fluid("matter").setUnlocalizedName("fluid." + Reference.MOD_ID + ".matter"), Material.water);
        setCreativeTab(CreativeTabs.tabMisc);
    }


    public void onEntityCollidedWithBlock(World world, int x1, int y1, int z1, Entity entity) {

        boolean look = true;
        while (look) {
            Random random = new Random();
            double x = entity.posX + random.nextDouble() * 20 - 10;
            double y = entity.posY + random.nextDouble() * 10 - 5;
            double z = entity.posZ + random.nextDouble() * 20 - 10;

            if (world.isAirBlock((int) x, (int) y, (int) z)) {


                entity.setPosition(x, y, z);
                look = false;

            }
        }
    }


    @Override
    public IIcon getIcon(int side, int meta) {
        return (side == 0 || side == 1) ? stillIcon : flowingIcon;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerBlockIcons(IIconRegister register) {
        stillIcon = register.registerIcon(Reference.MOD_ID + ":" + "water_still");
        flowingIcon = register.registerIcon(Reference.MOD_ID + ":" + "water_flow");
    }

    @Override
    public boolean canDisplace(IBlockAccess world, int x, int y, int z) {
        if (world.getBlock(x, y, z).getMaterial().isLiquid()) return false;
        return super.canDisplace(world, x, y, z);
    }

    @Override
    public boolean displaceIfPossible(World world, int x, int y, int z) {
        if (world.getBlock(x, y, z).getMaterial().isLiquid()) return false;
        return super.displaceIfPossible(world, x, y, z);
    }

}